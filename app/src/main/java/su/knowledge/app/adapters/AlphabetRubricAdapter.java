package su.knowledge.app.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import su.knowledge.app.MainActivity;
import su.knowledge.app.R;
import su.knowledge.app.fragments.ArticleFragment;


public class AlphabetRubricAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<String> list;
    Activity activity;

    public AlphabetRubricAdapter(ArrayList<String> articles, Activity activity) {
        list = articles;
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_articles_list, parent, false);
        TextView title = (TextView) view.findViewById(R.id.list_cats_item_title);
        TextView separ = (TextView) view.findViewById(R.id.list_cats_item_separator);
        return new AlphabetArticlesViewHolder(view, title, separ);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        AlphabetArticlesViewHolder viewHolder = (AlphabetArticlesViewHolder) holder;
        viewHolder.separatorTextView.setText("⚫");
        viewHolder.titleTextView.setText(list.get(position));
        viewHolder.titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.loadFragment(new ArticleFragment(), list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AlphabetArticlesViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView separatorTextView;

        public AlphabetArticlesViewHolder(View itemView, TextView titleTextView, TextView separatorTextView) {
            super(itemView);
            this.titleTextView = titleTextView;
            this.separatorTextView = separatorTextView;
        }

    }
}
