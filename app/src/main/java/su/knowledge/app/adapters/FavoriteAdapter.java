package su.knowledge.app.adapters;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import su.knowledge.app.ListLab;
import su.knowledge.app.MainActivity;
import su.knowledge.app.R;
import su.knowledge.app.fragments.ArticleFragment;

public class FavoriteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<String> list;
    Activity activity;

    public FavoriteAdapter(ArrayList<String> articles, Activity activity) {
        list = articles;
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_favorite, parent, false);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        actionBar.setTitle(R.string.favorite);
        TextView title = (TextView) view.findViewById(R.id.list_fav_item_title);
        TextView separ = (TextView) view.findViewById(R.id.list_fav_item_separator);
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.list_fav_item_image);
        Drawable drawable = imageButton.getDrawable();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        return new FavoriteArticlesViewHolder(view, title, separ, imageButton);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final FavoriteArticlesViewHolder viewHolder = (FavoriteArticlesViewHolder) holder;
        viewHolder.separatorTextView.setText("⚫");
        viewHolder.titleTextView.setText(list.get(position));
        viewHolder.titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.loadFragment(new ArticleFragment(), list.get(position));
            }
        });

        viewHolder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListLab.getListLab(getActivity()).favoriteListDel(viewHolder.titleTextView.getText().toString());
                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class FavoriteArticlesViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView separatorTextView;
        public ImageButton imageButton;

        public FavoriteArticlesViewHolder(View itemView, TextView titleTextView, TextView separatorTextView, ImageButton imageButton) {
            super(itemView);
            this.titleTextView = titleTextView;
            this.separatorTextView = separatorTextView;
            this.imageButton = imageButton;
        }

    }
}
