package su.knowledge.app;


import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class RestUtils {


    public static String httpRequest(String urladress) throws IOException {
        StringBuilder builder = new StringBuilder("");
        URL url = new URL(urladress);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        connection.disconnect();
        connection.getInputStream().close();
        bufferedReader.close();
        return builder.toString();
    }

    public static void htmlToListRubrics(String html, ArrayList<String> list) {
        try {
            JSONArray jsonArray = new JSONArray(html);
            for (int i = 0; i < jsonArray.length(); i++) {
                String tolist = jsonArray.getJSONObject(i).getString("name");
                tolist = tolist.replaceAll("&quot;", "");
                list.add(tolist);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String htmlToClearHTML(String html) {
        try {
            JSONObject jsonObject = new JSONObject(html);
            String text = jsonObject.getString("text");
            text = text.replaceAll("BASE_PATH", "http://knowledge.su/");
            return text;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String toBase64(String toBase) {
        try {
            byte[] data = toBase.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT).replace("\n", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
