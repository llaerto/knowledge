package su.knowledge.app.fragments;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import su.knowledge.app.MainActivity;
import su.knowledge.app.R;

public class AlphabetFragment extends Fragment {
    private GridView gridView;
    private ArrayList<String> myList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        myList = new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.Alphabet)));
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alphabet, container, false);AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        actionBar.setTitle(R.string.alphabet);
        gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setAdapter(new MyGridAdapter(myList));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity activity = (MainActivity) getActivity();
                activity.loadFragment(new AlphabetRubricFragment(), position);

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        Drawable drawable2 = menu.findItem(R.id.action_search).getIcon();
        if (drawable2 != null) {
            drawable2.mutate();
            drawable2.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            ((MainActivity) getActivity()).loadFragment(new SearchFragment(), -1);
        }
        return super.onOptionsItemSelected(item);
    }

    public class MyGridAdapter extends ArrayAdapter<String> {
        public MyGridAdapter(ArrayList<String> list) {
            super(getActivity(), 0, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.model_az, null);

            String AZ = myList.get(position);
            TextView textView = (TextView) convertView.findViewById(R.id.list_az_title);
            textView.setText(AZ);

            return convertView;
        }
    }
}
