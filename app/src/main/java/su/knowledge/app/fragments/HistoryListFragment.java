package su.knowledge.app.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import su.knowledge.app.ListLab;
import su.knowledge.app.R;
import su.knowledge.app.adapters.AlphabetRubricAdapter;


public class HistoryListFragment extends Fragment {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    AlphabetRubricAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        actionBar.setTitle(R.string.history);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_history);
        manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter = new AlphabetRubricAdapter(ListLab.getListLab(getActivity()).getHistoryList(), getActivity());
        recyclerView.setAdapter(adapter);
        return rootView;
    }
}
