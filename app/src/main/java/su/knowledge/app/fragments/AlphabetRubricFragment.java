package su.knowledge.app.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import su.knowledge.app.MainActivity;
import su.knowledge.app.R;
import su.knowledge.app.RestUtils;
import su.knowledge.app.adapters.AlphabetRubricAdapter;

public class AlphabetRubricFragment extends Fragment {
    private static final String URL_BEGIN = "http://knowledge.su/api.php?key=app_android_1&mode=letter&name=";
    private static final String FORMAT = "&format=json";
    private static final String PAGE = "&page=";

    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    AlphabetRubricAdapter adapter;
    ArrayList<String> list;
    int pageNum;
    int letter;
    EditText inputText;
    ProgressDialog progressDialog;
    ArrayList<String> myList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.progress_title));
        progressDialog.setMessage(getString(R.string.progress_message));
        progressDialog.show();
        pageNum = 0;
        letter = getArguments().getInt(MainActivity.FRAGM_CODE) + 1;
        list = new ArrayList<>();
        myList = new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.Alphabet)));
        doAsync();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_alphabet_rubric, container, false);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        actionBar.setTitle(getString(R.string.alphabet_rubric) + myList.get(letter - 1));
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_alphabet_rubric);
        manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter = new AlphabetRubricAdapter(list, getActivity());
        recyclerView.setAdapter(adapter);
        final ImageButton leftNarrow = (ImageButton) rootView.findViewById(R.id.image_narrow_left);
        final ImageButton rightNarrow = (ImageButton) rootView.findViewById(R.id.image_narrow_right);
        leftNarrow.setEnabled(false);
        leftNarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNum == 1) {
                    leftNarrow.setEnabled(false);
                }
                pageNum -= 1;
                doAsync();
                rightNarrow.setEnabled(true);

            }
        });
        rightNarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNum > 0) {
                    leftNarrow.setEnabled(true);
                }
                if (list.size() < 100) {
                    rightNarrow.setEnabled(false);
                }
                pageNum += 1;
                doAsync();
            }
        });
        inputText = (EditText) rootView.findViewById(R.id.page_edittext);
        inputText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    pageGo();
                    return true;
                }
                return false;
            }
        });
        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.page_go);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageGo();
            }
        });
        return rootView;
    }

    private class AsyncLoad extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return RestUtils.httpRequest(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("null")) {
                Toast.makeText(getActivity(), getString(R.string.nothing_longer), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                return;
            }
            list.clear();
            RestUtils.htmlToListRubrics(s, list);
            if (adapter.getItemCount() > 0) {
                adapter.notifyItemRangeChanged(0, list.size());
                int pageNumView = pageNum + 1;
                inputText.setText(pageNumView + "");
                progressDialog.dismiss();
            } else {
                Toast.makeText(getActivity(), getString(R.string.nothing_longer), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

        }
    }

    private void doAsync() {
        progressDialog.show();
        new AsyncLoad().execute(URL_BEGIN + letter + FORMAT + PAGE + pageNum);
    }

    private void pageGo() {
        if (inputText.length() > 0) {
            int num = Integer.parseInt(inputText.getText().toString());
            if (num > 0) {
                pageNum = num - 1;
                doAsync();
            }
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(inputText.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
