package su.knowledge.app.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import su.knowledge.app.ListLab;
import su.knowledge.app.R;
import su.knowledge.app.adapters.FavoriteAdapter;

public class FavoriteListFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    FavoriteAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorite, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_favorite);
        manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter = new FavoriteAdapter(ListLab.getListLab(getActivity()).getFavoriteList(), getActivity());
        recyclerView.setAdapter(adapter);
        return rootView;
    }
}
