package su.knowledge.app.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Arrays;

import su.knowledge.app.ListLab;
import su.knowledge.app.MainActivity;
import su.knowledge.app.R;
import su.knowledge.app.RestUtils;

public class ArticleFragment extends Fragment {
    private static final String URL_BEGIN = "http://knowledge.su/api.php?key=app_android_1&mode=term&name=";
    private static final String FORMAT = "&format=json";
    String name;
    String htmlCode;
    WebView webView;
    ProgressDialog progressDialog;
    boolean favorite;
    MenuItem bookmark;
    MenuItem delbookmark;
    private GoogleApiClient googleApiClient;
    static final Uri APP_URI = Uri.parse("android-app://su.knowledge.app/http/knowledge.su/");
    static final Uri WEB_URL = Uri.parse("http/knowledge.su/");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.progress_title));
        progressDialog.setMessage(getString(R.string.progress_message));
        progressDialog.show();
        name = getArguments().getString(MainActivity.FRAGM_CODE);
        if (!ListLab.getListLab(getActivity()).getFavoriteList().contains(name)) {
            ListLab.getListLab(getActivity()).historyListAdd(name);
        }
        favorite = ListLab.getListLab(getActivity()).getFavoriteList().contains(name);
        htmlCode = "";
        AsyncLoad asyncLoad = new AsyncLoad();
        asyncLoad.execute(URL_BEGIN + RestUtils.toBase64(name) + FORMAT);
        googleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
        Action viewAction = Action.newAction(Action.TYPE_VIEW, name, WEB_URL, APP_URI);
        AppIndex.AppIndexApi.start(googleApiClient, viewAction);
    }

    @Override
    public void onStop() {
        Action viewAction = Action.newAction(Action.TYPE_VIEW, name, WEB_URL, APP_URI);
        AppIndex.AppIndexApi.end(googleApiClient, viewAction);
        googleApiClient.disconnect();
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        actionBar.setTitle(name);
        webView = (WebView) view.findViewById(R.id.webview);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.article, menu);
        bookmark = menu.findItem(R.id.action_bookmark);
        delbookmark = menu.findItem(R.id.action_del_bookmark);
        if (favorite) {
            bookmark.setVisible(false);
        } else {
            delbookmark.setVisible(false);
        }
        Drawable drawable = menu.findItem(R.id.action_bookmark).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        Drawable drawable2 = menu.findItem(R.id.action_del_bookmark).getIcon();
        if (drawable2 != null) {
            drawable2.mutate();
            drawable2.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (favorite) {
            bookmark.setVisible(false);
            delbookmark.setVisible(true);
        } else {
            delbookmark.setVisible(false);
            bookmark.setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmark:
                favorite = true;
                getActivity().invalidateOptionsMenu();
                ListLab.getListLab(getActivity()).favoriteListAdd(name);
                return true;
            case R.id.action_del_bookmark:
                favorite = false;
                ListLab.getListLab(getActivity()).favoriteListDel(name);
                getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class AsyncLoad extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return RestUtils.httpRequest(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            htmlCode = RestUtils.htmlToClearHTML(s);
            if (htmlCode.length() > 0) {
                htmlCode = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + htmlCode;
                webView.loadDataWithBaseURL("file:///android_asset/", htmlCode, "text/html", "UTF-8", null);
                progressDialog.dismiss();
            } else {
                htmlCode = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + "<center>" + getString(R.string.no_article_found) + "</center>";
                webView.loadDataWithBaseURL("file:///android_asset/", htmlCode, "text/html", "UTF-8", null);
                progressDialog.dismiss();
            }
        }
    }
}
