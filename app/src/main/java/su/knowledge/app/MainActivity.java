package su.knowledge.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;

import su.knowledge.app.fragments.AboutFragment;
import su.knowledge.app.fragments.AlphabetFragment;
import su.knowledge.app.fragments.AlphabetRubricFragment;
import su.knowledge.app.fragments.FavoriteListFragment;
import su.knowledge.app.fragments.HistoryListFragment;
import su.knowledge.app.fragments.SearchFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String FRAGM_CODE = "su.knowledge.app.mainactivity.fragment_code";
    public static final int NON_LIST_FRAGMENT = -1;
    public static final int FAVORITES_FRAGMENT = 101;
    public static final int HISTORY_FRAGMENT = 102;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        toolbar.setSubtitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        toolbar.setTitle(R.string.short_name);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        loadFragment(new AlphabetFragment(), NON_LIST_FRAGMENT);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ListLab.getListLab(this).saveToJson();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (notFirstLaunch()) {
            ListLab.getListLab(this).readFromJson();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_alphabet:
                loadFragment(new AlphabetFragment(), NON_LIST_FRAGMENT);
                break;
            case R.id.nav_favorites:
                loadFragment(new FavoriteListFragment(), FAVORITES_FRAGMENT);
                break;
            case R.id.nav_history:
                loadFragment(new HistoryListFragment(), HISTORY_FRAGMENT);
                break;
            case R.id.nav_search:
                loadFragment(new SearchFragment(), NON_LIST_FRAGMENT);
                break;
            case R.id.nav_site:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getString(R.string.site_url)));
                startActivity(intent);
                break;
            case R.id.nav_about:
                loadFragment(new AboutFragment(), NON_LIST_FRAGMENT);
                break;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadFragment(Fragment myFragment, int fragmentConst) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = myFragment;
        if (fragment instanceof AlphabetRubricFragment) {
            navigationView.getMenu().getItem(0).setChecked(false);
        }
        if (fragmentConst != -1) {
            Bundle args = new Bundle();
            args.putInt(FRAGM_CODE, fragmentConst);
            fragment.setArguments(args);
        }
        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).
                addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();

    }

    public void loadFragment(Fragment myFragment, String name) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = myFragment;
        if (fragment instanceof AlphabetRubricFragment) {
            navigationView.getMenu().getItem(0).setChecked(false);
        }
        Bundle args = new Bundle();
        args.putString(FRAGM_CODE, name);
        fragment.setArguments(args);

        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).
                addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
    }

    public boolean notFirstLaunch() {
        File file = new File(getApplicationContext().getFilesDir().getAbsolutePath() + "/mylist.json");
        if (file.exists())
            return true;
        else
            return false;
    }

}
