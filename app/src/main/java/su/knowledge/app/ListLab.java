package su.knowledge.app;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class ListLab {
    private static ListLab listLab;
    private Context context;
    private ArrayList<String> historyList;
    private ArrayList<String> favoriteList;

    private ListLab(Context context) {
        this.context = context;
        historyList = new ArrayList<>();
        favoriteList = new ArrayList<>();
    }

    public static ListLab getListLab(Context context) {
        if (listLab == null)
            listLab = new ListLab(context);
        return listLab;
    }

    public void historyListAdd(String str) {
        historyList.add(str);
    }

    public ArrayList<String> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(ArrayList<String> list) {
        historyList = list;
    }

    public void favoriteListAdd(String str) {
        favoriteList.add(str);
    }

    public void favoriteListDel(String str) {
        favoriteList.remove(str);
    }

    public void setFavoriteList(ArrayList<String> list) {
        favoriteList = list;
    }

    public ArrayList<String> getFavoriteList() {
        return favoriteList;
    }

    public void saveToJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(favoriteList);
        try {
            FileWriter fileWriter = new FileWriter(context.getFilesDir().getAbsolutePath() + "/mylist.json");
            fileWriter.write(json);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromJson() {
        String json = "";
        try {
            FileReader fileReader = new FileReader(context.getFilesDir().getAbsolutePath() + "/mylist.json");
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                json = json + line + "\n";
            }
            fileReader.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> myLists = new Gson().fromJson(json, listType);
        setFavoriteList(myLists);
    }


}
