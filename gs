[33mcommit 1d5eb96cc637feb0aaecb416d3a31eec15c3103e[m
Author: LaerTo <llaerto@gmail.com>
Date:   Fri Nov 20 19:25:44 2015 +0200

    Added gridview (not internet active), implemented fragment transitions

[33mcommit f4645fa225d8bbb459515f41258ae9a4efff978c[m
Author: LaerTo <llaerto@gmail.com>
Date:   Fri Nov 20 18:42:50 2015 +0200

    Implemented About fragment and Site button

[33mcommit f840952595111e6073d5261bb2f4d15e3d758497[m
Author: LaerTo <llaerto@gmail.com>
Date:   Fri Nov 20 18:17:49 2015 +0200

    Implemented search edit text in nav drawer (and button)

[33mcommit 56d681262c2061ce7a82db34bdabcc5e2dadcc7c[m
Author: LaerTo <llaerto@gmail.com>
Date:   Fri Nov 20 18:00:27 2015 +0200

    Created fragments base arhitecture, implemented fragments opening from nav drawer

[33mcommit 95e8508471b837361ff0ac966a77644a2ef7e052[m
Author: LaerTo <llaerto@gmail.com>
Date:   Wed Nov 18 17:21:33 2015 +0200

    First commit, nav drawer implemented, colors\resourse
